def vowel_swapper(string):
    # ==============
    # Your code here
    list1 = list(string)
    #print(list1)
    acount = False
    ecount = False
    icount = False
    ocount = False
    ucount = False
    #print(len(list1))

    for i in range(0, len(list1)):
        #print(i)

        if list1[i] == "a" or list1[i] == "A":
            #print(f"character = {list1[i]} count = {acount} index = {i}")
            if acount:
                list1[i] = list1[i].replace("a", "4")
                list1[i] = list1[i].replace("A", "4")
                acount = False
            else:
                acount = True
        elif list1[i] == "e" or list1[i] == "E":
            if ecount:
                list1[i] = list1[i].replace("e", "3")
                list1[i] = list1[i].replace("E", "3")
                ecount = False
            else:
                ecount = True
        elif list1[i] == "i" or list1[i] == "I":
            if icount:
                list1[i] = list1[i].replace("i", "!")
                list1[i] = list1[i].replace("I", "!")
                icount = False
            else:
                icount = True
        elif list1[i] == "o" or list1[i] == "O":
            if ocount:
                list1[i] = list1[i].replace("o", "ooo")
                list1[i] = list1[i].replace("O", "ooo")
                ocount = False
            else:
                ocount = True
        elif list1[i] == "u" or list1[i] == "U":
            if ucount:
                list1[i] = list1[i].replace("u", "|_|")
                list1[i] = list1[i].replace("U", "|_|")
                ucount = False
            else:
                ucount = True

    finalstring = " "
    for elem in list1:
        finalstring = finalstring + elem

    return finalstring
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console

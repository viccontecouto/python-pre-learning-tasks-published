def calculator(a, b, operator):
    # ==============
    # Your code here
    ans = 0
    if operator == "+":
        ans = a+b
    elif operator == "-":
        ans = a-b
    elif operator == "*":
        ans = a*b
    elif operator == "/":
        ans = a/b
    string1 = bin(int(ans))[2:]
    return string1
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
